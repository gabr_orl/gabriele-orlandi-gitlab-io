---
title: Contact
permalink: /impressum/
aside: true
---


{% include button.html text="Email me" link="mailto:heidi@seibold.co" icon="email" %}
{% include button.html text="Connect with me on Twitter (@HeidiBaya)" link="https://twitter.com/HeidiBaya" icon="twitter" color="#1DA1F2"%}
{% include button.html text="Check out my YouTube channel" link="https://www.youtube.com/channel/UCjWGWyuJLe7hxHOhkrjnKOg" icon="youtube" color="#FF0000"%}


{% include figure.html image="../assets/images/W-consulting.png" alt="Watering light-bulb-plant" width=300 position="right" %}
