---
layout: post
title:  "Hello world!"
date:   2021-12-18
categories: 
 - hello
 - about me
feature_image: "https://picsum.photos/2560/600?image=872"
---

This is the very first post on my new website's blog. 

This website is intended as a landing page for my professional life.  So if you
are here and reading this: I am looking forward to make business with you!

I've had a blog before, but it has gotten a bit old. Interested? You can find
it here: http://heidiseibold.github.io/

I created this website myself with the help of [Jekyll](https://jekyllrb.com/)
and the beautiful [alembic theme](https://alembic.darn.es/).

