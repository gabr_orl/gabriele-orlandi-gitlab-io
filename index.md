---
feature_text: |
  ## Heidi Seibold
  Reproducibility, Data Science, & Community
feature_image: "../assets/images/Header-Image.png"
excerpt: "I can help you with reproducibility, data science, and community"
---


{% include figure.html image="../assets/images/Heidi_round.png" alt="Pic of Heidi" width=200 %}

### Services:

{% include figure.html image="../assets/images/W-workshops.png"  alt="Workshop icon" width=90 position="left"%} 
##### Workshops + Training
Want to learn about reproducibility and/or data science in a fun and creative workshop? Great, because that is something I do well and enjoy. 


{% include figure.html image="../assets/images/W-consulting.png"  alt="Workshop icon" width=90 position="left" %}
##### Consulting
Let me help you get the best out of your projects. May it be about data science, open science or reproducibility.


{% include figure.html image="../assets/images/W-talks.png"  alt="Workshop icon" width=90 position="left" %}
##### Talks
Book me as a speaker in live or online events. I am also happy to moderate your event.


{% include figure.html image="../assets/images/W-media.png"  alt="Workshop icon" width=90 position="left"%}
##### Media
Need videos or audio for your project or classroom? I am an experienced [podcast](./podcasts)
host and have created numerous videos.

I am open to extend my services to other projects and tasks. 

All services are available in German and English language. 

{% include button.html text="Get in touch!" link="/impressum/" %}


