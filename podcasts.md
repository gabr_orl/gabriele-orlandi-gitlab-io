---
feature_text: |
  ## Podcasts
  I am the host of two podcasts. Check them out!
---

<a href="https://reboot-academia.podigee.io/" style="float: left;" target="_blank" rel="noopener noreferrer">
  <img src="../assets/images/Podcast_RebootAcademia.png" 
       alt=">reboot academia podcast" 
       width="300" position="left" />
</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="https://anchor.fm/opensciencestories" target="_blank" rel="noopener noreferrer">
  <img src="../assets/images/Podcast_OpenScienceStories.png" 
       alt="Open Science Stories podcast" 
       width="300" position="right" />
</a>
